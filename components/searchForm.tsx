import { Component, ChangeEvent } from "react";
import Input from "./input";
import Button from "./button";
import style from "./searchForm.module.scss";
import RepositoryResults from "./repositoryResult";
import { getRepositories } from "../pages/client";

interface IMyComponentErrors {
  repositoryNameError: string;
}

interface IMyComponentProps {
  repositoryNameDefaultValue: string;
}

interface IMyComponentState {
  repositoryName: string;
  formIsSend: boolean;
  repositoriesData: any;
  errors: IMyComponentErrors;
  isLoading: boolean;
}

const validateForm = (errors: IMyComponentErrors): boolean => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

const validRepositoryNameRegex = RegExp(/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/i);

export default class SignUpFormContainer extends Component<
  IMyComponentProps,
  IMyComponentState
> {
  constructor(props: IMyComponentProps) {
    super(props);
    this.state = {
      repositoryName: this.props.repositoryNameDefaultValue,
      formIsSend: false,
      repositoriesData: null,
      errors: {
        repositoryNameError: "",
      },
      isLoading: false
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleChangeRepositoryName = this.handleChangeRepositoryName.bind(this);
  }

  handleChangeRepositoryName(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (!validRepositoryNameRegex.test(value)) {
      errors.repositoryNameError = "Invalid repository name";
    } else if (!value) {
      errors.repositoryNameError = "Repository name is required";
    } else {
      errors.repositoryNameError = "";
    }

    this.setState({ errors, repositoryName: value });
  }

  handleFormSubmit(event) {
    event.preventDefault();
    const { repositoryName } = this.state;
    let errors = this.state.errors;

    if (!repositoryName) {
      errors.repositoryNameError = "Repository name is required";
    }

    this.setState({ errors });

    if (!validateForm(this.state.errors)) {
      return;
    } else {
      this.setState({ formIsSend: true, isLoading: true });

      getRepositories(this.state.repositoryName).then((results) => {
        this.setState({ repositoriesData: results.data.search.nodes, isLoading: false });
      });
    }
  }

  render() {
    const { errors } = this.state;

    return (
      <div>
      { this.state.isLoading ? <h2 className={style.searchingText}>Searching...</h2> : !this.state.repositoriesData ? (
        this.state.formIsSend ? <h2 className={style.searchingText}>No results found</h2> :
      <form
        aria-label="Search repositories by name"
        autoComplete="off"
        onSubmit={this.handleFormSubmit}
        className = {style.formSearchRepository}
      >
        <Input
          type={"text"}
          title={"Repository name:"}
          name={"repositoryName"}
          placeholder={"Enter name of repository"}
          value={this.state.repositoryName}
          error={errors.repositoryNameError.length > 0}
          errorMessage={errors.repositoryNameError}
          onChange={this.handleChangeRepositoryName}
          required
        />
        <Button
          onClick={this.handleFormSubmit}
          title={"Search repository in Github by name"}
          children={"Search"}
        />
      </form>
      ) : <RepositoryResults repositoriesData={this.state.repositoriesData} />}
      </div>
    );
  }
}
