import React from "react";
import style from "./repositoryResults.module.scss";

interface IRepositoryComponentProps {
  repositoriesData: any;
}

interface IRepositoryComponentState {
  detailsAreOpen: {
    [key: number]: boolean;
  };
}

export default class RepositoryResults extends React.Component<IRepositoryComponentProps, IRepositoryComponentState> {
  constructor(props: IRepositoryComponentProps) {
    super(props);
    this.state = {
      detailsAreOpen: {}
    };

    this.showDetails = this.showDetails.bind(this);
    this.setDelay = this.setDelay.bind(this);
  }

  showDetails(event, id): void {
    this.setState({ 
      detailsAreOpen: { [id]: !this.state.detailsAreOpen[id] }
    });
  }

  setDelay(i) {
    return {
      animationDelay:  (i * 200) + 'ms'
    }
  }

  render() {
    return (
      <section className={style.resultsList}>
        { this.props.repositoriesData.length == 0 ? (<p>No results found.</p>) : this.props.repositoriesData.map((repo, index) => (
          <div key={index} className={style.accordion} style={this.setDelay(index)}>
            <header onClick={(event) => { this.showDetails(event, index) }} className={`${style.accordionHeader} ${this.state.detailsAreOpen[index] ? style.accordionDetailsOpen : style.accordionDetailsClose}`}>
              <h2 className={style.accordionHeaderTitle}>
                { repo.name && <span><b>Name: </b>{repo.name} </span> }
                { repo.owner && repo.owner.login && <span><b>Owner: </b>{repo.owner.login} </span> }
                { repo.stargazerCount && <span><b>Number of stars (total): </b>{repo.stargazerCount} </span> }
                { repo.primaryLanguage && repo.primaryLanguage.name && <span><b>Primary language: </b>{repo.primaryLanguage.name} </span> }
              </h2>
            </header>
            { this.state.detailsAreOpen[index] ? (
            <div className={style.moreDetails}>
              { repo.issues && repo.issues.totalCount && <div><b>Total issues: </b>{repo.issues.totalCount}</div> }
              { repo.languages && repo.languages.totalCount && <div><b>Total language: </b>{repo.languages.totalCount}</div> }
              { repo.shortDescriptionHTML && <div><b>Description: </b> <span dangerouslySetInnerHTML={{__html: repo.shortDescriptionHTML }}/></div> }
              { repo.stargazers && repo.stargazers.totalCount && <div><b>Stargazers: </b>{repo.stargazers.totalCount}</div> }
              { repo.updatedAt && <div><b>Last update: </b>{repo.updatedAt}</div> }
              { repo.watchers && repo.watchers.totalCount && <div><b>Watchers: </b>{repo.watchers.totalCount}</div> }
            </div>
            ) : null }
          </div>
        ))}
      </section>
    );
  }   
}
