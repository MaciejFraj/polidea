import Layout from "../components/layout"
import SearchForm from "../components/searchForm"

export default function Home() {
  return  (
    <Layout pageTitle="React App" headerTitle="Search repositories on Github">
      <SearchForm repositoryNameDefaultValue='' />
    </Layout>
  );
}
