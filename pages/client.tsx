import {
  ApolloClient,
  ApolloLink,
  createHttpLink,
  gql,
  InMemoryCache,
  NormalizedCacheObject
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

const httpLink: ApolloLink = createHttpLink({
  uri: "https://api.github.com/graphql",
});

const authLink: ApolloLink = setContext((_, { headers }) => {
  const token = "703e20d6f37c253ee90e85962c2266700491d4ad";
  
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const GET_REPOSITORIES_BY_NAME = gql`query findRepositories($repositoryName: String!) {
  search(first: 10, query: $repositoryName, type: REPOSITORY) {
    nodes {
      ... on Repository {
        name,
        owner {
          login
        }
        primaryLanguage {
          name
        },
        stargazers {
          totalCount
        },
        stargazerCount,
        languages(first: 20, orderBy: {field: SIZE, direction: ASC} ) {
          totalCount
          nodes {
            name
          }
        },
        issues {
          totalCount
        }
        shortDescriptionHTML,
        updatedAt,
        watchers {
          totalCount
        }
      }
    }
  }
}`;

export const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export async function getRepositories(repositoryName: string) {
  const res: any =  await client.query({
    query: GET_REPOSITORIES_BY_NAME,
    variables: { repositoryName }
  });

  const results = await res;
  return results;
}
